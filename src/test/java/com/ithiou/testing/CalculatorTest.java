package com.ithiou.testing;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Timeout;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.ValueSource;

import static org.junit.jupiter.api.Assertions.*;
import static org.assertj.core.api.Assertions.*;

import java.time.Duration;
import java.time.Instant;



import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class CalculatorTest {
	private Calculator calculator;
	private static Instant starteedAt;

	@BeforeAll
	static void initStartingTime(){
		System.out.println("Appel avant tous les tests!!!!");
		starteedAt=Instant.now();
	} 

	@AfterAll
	static void showTestDuration(){
		System.out.println("Appel apres tous les tests!!!!!");
		Instant endAt= Instant.now();
		long duration= Duration.between(starteedAt, endAt).toMillis();
		System.out.println("duree des tests:" + duration);

	}

	@BeforeEach
	void initCalculator(){
		calculator=new Calculator();
		System.out.println("Appel avant chaque test!!!!");
	}

	@AfterEach
	void undefCalculator(){
		System.out.println("Apres chaque test");
		calculator=null;
	}

	@Test
	void testAddTwoPositiveNumbers() {
		// ARRANGE
		final int a = 2;
		final int b = 3;

		// ACT
		final int somme = calculator.add(a, b);

		// ASSERT
		//assertEquals(5, somme);
		assertThat(somme).isEqualTo(5);
		
	}

	@Test
	void testMultipleNumbers() {
		// ARRANGE
		final int a = 2;
		final int b = 3;

		// ACT
		final int multi = calculator.multi(a, b);

		// ASSERT
		//assertEquals(6, multi);
		assertThat(multi).isEqualTo(6);
		
	}

	@ParameterizedTest(name= "{0} * 0 doit etre egal a 0")
	@ValueSource(ints={1, 2, 42, 1001, 5089})
	public void testMultiNumberForZero(int arg){
		//ARRANGE rien na faire
		//ACT
		final int result=calculator.multi(0, arg);
		//ASSERT
		//assertEquals(0, result);
		assertThat(result).isEqualTo(0);

	}

	@ParameterizedTest(name= "{0} + {1} doit etre egal a {2}")
	@CsvSource({"1,1,2", "2,3,5", "23,12,35"})
	public void testSommesNumberForArgs(int arg1, int arg2, int result){
		//ARRANGE rien na faire
		//ACT
		final int waiting=calculator.add(arg1, arg2);
		//ASSERT
		//assertEquals(waiting, result); //assert junit
		assertThat(waiting).isEqualTo(result); //assertJ

	}

	@Test
	@Timeout(1)
	public void longTest(){
		//ARRANGE rien na faire
		//ACT
		calculator.longTest();
		//ASSERT		
	}

}
